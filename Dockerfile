FROM archlinux:base

RUN pacman -Syu --noconfirm hcloud grep awk sed

ENTRYPOINT ["/usr/bin/hcloud"]
CMD ["--help"]
