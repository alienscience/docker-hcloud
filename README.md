# docker-hcloud

Hetzner cloud cli in a docker image.

```
docker pull registry.gitlab.com/alienscience/docker-hcloud
```

## Running

The environment variable `$HCLOUD_TOKEN` needs to be set with
a valid token from Hetzner cloud.

For example:

```
docker run --rm --env HCLOUD_TOKEN=abc123 registry.gitlab.com/alienscience/docker-hcloud image list
```

The image also contains grep, awk and sed so that it can be as a container in a continuous
deployment pipeline:

```
hcloud image list | grep my-image | awk '{print $1}' | xargs -n1 hcloud image delete
```

If you run as part of a CI or CD pipeline set the entrypoint to `[""]`.
